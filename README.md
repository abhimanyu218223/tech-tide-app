# InfraMinds TechTide CICD Workshop

## Project forking workflow
Whenever possible, it’s recommended to work in a common Git repository and use branching strategies to manage your work. However, if you do not have write access for the repository you want to contribute to, you can create a fork.

A fork is a personal copy of the repository and all its branches, which you create in a namespace of your choice.

Create a fork of the below repo
```
https://gitlab.com/inframinds7/tech-tide-app.git
```

CI/CD variables: use these environment variables to configure job behavior in the configuration file and in script commands.

Setup AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY in Settings > CI/CD > Variables

## CICD Pipeline

Create a .gitlab-ci.yml file at the root of your repository. This file is where you define the CI/CD jobs.

You can use include to include external YAML files in your CI/CD jobs.
```
include:
  - template: Terraform/Base.gitlab-ci.yml  # https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Terraform/Base.gitlab-ci.yml
```
Set the root folder of your terraform code as a variable.
```
variables:
  TF_ROOT: infrastructure
```

The most common pipeline configurations group jobs into stages. Jobs in the same stage can run in parallel, while jobs in later stages wait for jobs in earlier stages to complete. If a job fails, the whole stage is considered failed and jobs in later stages do not start running.

Add the below stages to yaml file

```
stages:
  - validate
  - test
  - build
  - deploy
  - cleanup
```

Add below code to create a job for terraform fmt. The terraform fmt command is used to rewrite Terraform configuration files to a canonical format and style. 
```
fmt:
  extends: .terraform:fmt
  needs: []
```

The terraform validate command validates the configuration files in a directory, referring only to the configuration and not accessing any remote services such as remote state, provider APIs, etc.

```
validate:
  extends: .terraform:validate
  needs: []
```

The terraform plan command creates an execution plan, which lets you preview the changes that Terraform plans to make to your infrastructure. 
```
build:
  extends: .terraform:build
  environment:
    name: $TF_STATE_NAME
    action: prepare
```

Manual job to apply the infrastructure changes.
Terraform takes the actions in the saved plan without prompting you for confirmation. 
```
deploy:
  extends: .terraform:deploy
  environment:
    name: $TF_STATE_NAME
    action: start
```

Terraform Destroy
```
destroy:
  extends: .terraform:destroy
```

Commit and push changes to repo.

## Setup the backend NodeJS App and the fraontend React App

Navigate to application-code/app-tier/README.md to see instructions on how to setup the MySQL database and NodeJS App

Navigate to application-code/web-tier/README.md to see instructions on how to setup the React App